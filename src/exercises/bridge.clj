(ns exercises.bridge)

(comment
  "Scoring bridge hands.

  This exercise was pulled from: http://www.eecs.berkeley.edu/~bh/ssch9/bridge.html

  A bridge hand contains 13 cards.
  Scoring rules:

  Points by type (high cards):

  Ace   => 4 points
  King  => 3 points
  Queen => 2 points
  Jack  => 1 point

  Points by distribution:

  Only 2 cards same suit           => 1 point
  Only 1 card of a particular suit => 2 points
  No cards in a particular suit    => 3 points

  Cards will be represented by strings where the first character is the card
  value and the last character is the card suit.
  For example: \"qc\" => queen of clubs, \"7s\" => 7 of spades, \"ah\" => ace of hearts.

  A hand will be represented by a vector of cards, i.e., [\"3h\" \"7d\" ...]

  The program will use the functions defined below and is defined in a 'bottom up'
  approach. The intent is that each subsequent function will leverage the function
  before it. Although beginning with small details like this is not normally how
  programs are described, it is done intentionally to define the program structure.
"
 )

(defn card-val
  "Returns the value of the passed card."
  [c]
  "IMPLEMENT ME")

(defn high-card-points
  "Returns the total number of points from high cards in the hand."
  [h]
  "IMPLEMENT ME")

(defn count-suit
  "Returns the number of cards of suit (s) in the hand (.h)"
  [s h]
  "IMPLEMENT ME")

(defn suit-counts
  "Returns a collection containing the number of spades, hearts, clubs and
  diamonds in the hand."
  [h]
  "IMPLEMENT ME")

(defn suit-dist-points
  "Returns the number of distribution points the suit count (n) is worth."
  [n]
  "IMPLEMENT ME")

(defn hand-dist-points
  "Returns the number of distribution points the hand (h) is worth."
  [h]
  "IMPLEMENT ME")

(defn bridge-val
  "Returns the total number of points the hand (h) is worth."
  [h]
  "IMPLEMENT ME")
