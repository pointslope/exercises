(ns exercises.tree)

(defn depth-first-preorder
  "Perform a preorder traversal of a tree, depth-first."
  [tree]
  "IMPLEMENT ME")

(defn depth-first-inorder
  "Perform an in-order traversal of a tree, depth-first."
  [tree]
  "IMPLEMENT ME")

(defn depth-first-postorder
  "Perform a post-order traversal of a tree, depth-first."
  [tree]
  "IMPLEMENT ME")
