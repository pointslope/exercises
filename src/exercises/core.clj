(ns exercises.core
  (:gen-class))

(defn -main
  "Nothing to see here at the moment"
  [& args]
  (println "Run 'lein midje' to see something of interest"))
