(ns exercises.tree-test
  (:use clojure.test
        midje.sweet
        exercises.tree))

(def tree [:f [:b [:a nil nil][:d [:c nil nil][:e nil nil]]][:g nil [:i [:h nil nil] nil]]])

(fact
  (depth-first-preorder tree) => [:f :b :a :d :c :e :g :i :h])

(fact
  (depth-first-inorder tree) => [:a :b :c :d :e :f :g :h :i])

(fact
  (depth-first-postorder tree) => [:a :c :e :d :b :h :i :g :f])
