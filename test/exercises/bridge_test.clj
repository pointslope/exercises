(ns exercises.bridge-test
  (:use clojure.test
        midje.sweet
        exercises.bridge))

;; card-val tests
(fact
  (card-val "ah") => 4)

(fact
  (card-val "kh") => 3)

(fact
  (card-val "qh") => 2)

(fact
  (card-val "jc") => 1)

(fact
  (card-val "7s") => 0)

;; high-card-points tests
(fact
  (high-card-points ["ah" "10s" "qh" "kc" "4c"]) => 9)

(fact
  (high-card-points ["as" "10s" "7s" "6s" "2s" "qh" "jh" "9h" "kc" "kd" "9d" "3d"]) => 13)

;; count-suit tests
(fact
  (count-suit \s ["as" "10s" "qh" "kc" "4c"]) => 2)

(fact
  (count-suit \c ["as" "10s" "7s" "6s" "2s" "qh" "jh" "9h" "kc" "4c" "kd" "9d" "3d"]) => 2)

(fact
  (count-suit \d ["3h" "7d" "ks" "3s" "10c" "qd" "8d" "9s" "4s" "10d" "7c" "4d" "2s"]) => 5)

;; suit-counts tests
(fact
  (suit-counts ["as" "10s" "qh" "kc" "4c"]) => [2 1 2 0])

(fact
  (suit-counts ["as" "10s" "7s" "6s" "2s" "qh" "jh" "9h" "kc" "4c" "kd" "9d" "3d"]) => [5 3 2 3])

(fact
  (suit-counts ["3h" "7d" "ks" "3s" "10c" "qd" "8d" "9s" "4s" "10d" "7c" "4d" "2s"]) => [5 1 2 5])

;; suit-dist-points test
(fact
  (suit-dist-points 2) => 1)

(fact
  (suit-dist-points 7) => 0)

(fact
  (suit-dist-points 0) => 3)

;; hand-dist-points tests
(fact
  (hand-dist-points ["as" "10s" "7s" "6s" "2s" "qh" "jh" "9h" "kc" "4c" "kd" "9d" "3d"]) => 1)

(fact
  (hand-dist-points ["3h" "7d" "ks" "3s" "10c" "qd" "8d" "9s" "4s" "10d" "7c" "4d" "2s"]) => 3)

;; bridge-val tests
(fact
  (bridge-val ["as" "10s" "6s" "7s" "2s" "qh" "jh" "9h" "kc" "4c" "kd" "9d" "3d"]) => 14)

(fact
  (bridge-val ["3h" "7d" "ks" "3s" "10c" "qd" "8d" "9s" "4s" "10d" "7c" "4d" "2s"]) => 8)
