(ns exercises.math-test
  (:use clojure.test
        midje.sweet
        exercises.math))

(fact
  (pascals-triangle 0) => [])

(fact 
  (pascals-triangle 1) => [[1]])

(fact 
  (pascals-triangle 2) => [[1]
                          [1 1]])

(fact
  (pascals-triangle 3) => [[1]
                          [1 1]
                         [1 2 1]])

(fact
  (pascals-triangle 4) => [[1]
                          [1 1]
                         [1 2 1]
                        [1 3 3 1]])

(fact
  (pascals-triangle 5) => [[1]
                          [1 1]
                         [1 2 1]
                        [1 3 3 1]
                       [1 4 6 4 1]])

(fact
  (pascals-triangle 6) => [[1]
                          [1 1]
                         [1 2 1]
                        [1 3 3 1]
                       [1 4 6 4 1]
                     [1 5 10 10 5 1]])
