# exercises

Perform some simple programming exercises/katas like:

- tree traversal
- Pascal's triangle
- Tower of Hanoi


## Installation

Clone the project. Dependencies are visible in project.clj,
but you'll need Clojure and Leiningen at a minimum.

## License

Copyright © 2012 Point Slope, LLC

Distributed under the Eclipse Public License, the same as Clojure.
